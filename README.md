# docker-flask-test

# build the docker image with Flask and Pillow
docker image build -t flask .

# run the docker
docker run -it --rm flask bash

# run app
python app.py
