# init a base image (Alpine is small Linux distro)
FROM python:3.6.1
# define the present working directory
WORKDIR /app
# copy the contents into the working dir
ADD . /app
#RUN pip install Flask
#RUN pip install Pillow
RUN pip install -r requirements.txt
# define the command to start the container
CMD ["python","app.py"]

